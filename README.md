# QUESTÃO 1

Primeiramente preciso saber quais os resultados esperados de cada solicitação.
Já que ao cadastrar um planeta e ele vai me dar a quantidade de filmes, preciso saber "Caminhos felizes", para reproduzir uma criação já tendo conhecimento do resultado para a consulta.

## Case 1: Cadastrar planeta
- Resultado esperado: Ao clicar em "Cadastrar" deve registrar as informações na tabela de acordo.

	1. Inserir as seguintes informações em "CADASTRO DE PLANETAS:
		- Nome: B
		- Clima: 2
		- Terreno: Y
	2. Clicar em "Cadastrar"
		- Após clicar em "Cadastar", deve registrar as seguintes informações na tabela:
			- ID <- Registro único automático.
			- Nome: B 
			- Clima: 2
			- Terreto: Y
			- Filmes: 2 <- Valor inserido na tabela após identificar o cadastro do planeta".

## Case 2: Excluir planeta
- Resultado esperado: Ao clicar em "Remover" deve excluir o registro do planeta selecionado
	1. Escolher um dos registros de planeta
	2. Clicar em "Remover". 

## Case 3: Consultar Planeta
- Resultado esperado: Ao digitar caracteres dentro do campo "Digite um nome ou ID", filtrar a tabela.

	1. Digitar uma das seguintes opções:
		- 124 (ID do registro)
		- B (Nome do filme) 
	2. Retornar o registro: 
		- Nome: B
		- Clima: 2
		- Terro: Y
		- Filmes: 2

## Case 5: Cadastro com campos vazios(Tentar cadastrar seguindo para cada uma das opções abaixo).
- Resultado esperado: De acordo com o que foi informado, o único campo obrigatório seria o "Nome" do planeta. Sendo assim quando não houver o campo "Nome" na hora de cadastrar o registro, não deve permitir inserir o registro.

	1. Todos os campos vazios
	2. Apenas o campos "Nome" vazio
	3. Apenas o campos "Clima" vazio
	4. Apenas o campos "Terreno" vazio

## Case 6: Limpar a consulta.
- Resultado esperado: Limpar o campo de "consulta", juntamente com o filtro na tabela.

	1. Inserir qualquer caractere no campo "Digite um nome ou ID".
		- Filtro será realizado
	2. Clicar em "Limpar".
	3. Trazer todos os registros da tabela.

# QUESTÃO 2

## A:
```sql
SELECT p.codigoBarras 
FROM produtos p
WHERE p.dataCadastro > (now() - interval '10' day);
```
## B:
```sql
SELECT o.nome 
FROM produtoInfo pi 
INNER JOIN origem o ON pi.idOrigem = o.id
WHERE (MONTH(pi.dataCadastro) = 3 AND  YEAR(pi.dataCadastro)= 2020)
OR (MONTH(pi.dataAtualizacao) = 3 AND  YEAR(pi.dataAtualizacao)= 2020);
```
## C:
```sql
UPDATE produtoInfo pi
SET pi.idFabricante = (SELECT f.id FROM fabricante f WHERE f.nome = 'JOAO')
WHERE 
pi.idOrigem = (SELECT o.id FROM origem o WHERE o.nome = 'DISTRIBUIDORA TESTE') 
AND 
pi.idFabricante = (SELECT f.id FROM fabricante f WHERE f.nome = 'MARIA');
```
## D:
```sql
SELECT p.codigoBarras, pi.descricao, f.nome, pi.codigoInterno
FROM produtoInfo pi
JOIN produto p on pi.IdProduto = p.id
JOIN fabricante f on pi.IdFabricante = f.id
JOIN origem o on pi.idOrigem = o.id
GROUP BY p.codigoBarras
ORDER BY o.preferencia DESC;

```
# QUESTÃO 3
## Realizei a automação com Selenium conforme solicitado. Realizei a automação de acordo com meu conhecimento atual e uma pequena pesquisa.
## Para rodar a automação seguir os seguintes passos:

- OBS: Necessário ter o node instalado para utilizar do gerenciador de pacotes npm e para rodar o script.
1. Baixe o projeto localmente

2. Dentro da pasta do projeto execute o comando
```
npm install
```

3. Após a conclusão, execute o comando:
```
node ./server.js
```

4. Verificar que a automação chega a pesquisa de mês alterando para a data de julho.

- OBS: Não finalizei os testes devido a falta de conhcimento atual.

# QUESTÃO 4

## A:
- O erro informado é devolvido quando inserimos um dado fora do padrão int(integer).
Ex: Se tentarmos inserir uma letra ("a") e enviar a requsição, será devolvido o erro em questão.
- [EVIDÊNCIA](https://drive.google.com/file/d/1nQBxhv71EFbgV0wCWPA02TwqBCUbQXg4/view?usp=sharing)

## B:
- O erro informado é quando é feita uma divisão por zero.
Ex: 1000 / 0
- [EVIDÊNCIA](https://drive.google.com/file/d/14C5z76Tjrd2JS7o0lp3iYFbNMtnxIX4I/view?usp=sharing)


