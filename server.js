const { Builder, By, Key } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

let options = new chrome.Options();
options.addArguments('--disable-dev-shm-usage');

let driver = new Builder()
  .forBrowser('chrome')
  .setChromeOptions(options)
  .build();

async function run() {
  try {
    await driver.get('https://globo.com');
    //Barra de pesquisa
    const searchBar = await driver.findElement(By.xpath('/html/body/header/div[2]/div[3]/div[2]/div/div/form/input'));
    await searchBar.sendKeys('futebol', Key.ENTER);
    //Filtrar Data
    const filter = await driver.findElement(By.xpath('/html/body/div[1]/div/div[1]/div/div/div[2]/div/a'))
    await filter.click();
    //Filtro personalizado
    const customFilter = await driver.findElement(By.xpath('/html/body/div[1]/div/div[1]/div/div/div[2]/div/ul/li[8]/span'));
    await customFilter.click();
    //Novembro
    let month = await driver.findElement(By.className('DayPickerNavigation_button DayPickerNavigation_button_1 DayPickerNavigation_button__default DayPickerNavigation_button__default_2 DayPickerNavigation_button__horizontal DayPickerNavigation_button__horizontal_3 DayPickerNavigation_button__horizontalDefault DayPickerNavigation_button__horizontalDefault_4 DayPickerNavigation_leftButton__horizontalDefault DayPickerNavigation_leftButton__horizontalDefault_5'));
    await month.click();
  } finally {
    await driver.sleep;
  }
}

run();